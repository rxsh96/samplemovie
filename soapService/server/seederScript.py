from django_seed import Seed
import random

import django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "tallerDaw.settings")
django.setup()

seeder = Seed.seeder()

from samplemovie.models import Plan, Cliente, Pelicula, ClientePelicula

seeder.add_entity(Plan, 3)
seeder.add_entity(Cliente, 50)
seeder.add_entity(Pelicula, 50)
seeder.add_entity(ClientePelicula, 50)

inserted_pks = seeder.execute()

seeder.add_entity(Plan, 50, {
    'tipo': lambda x: seeder.faker.word(),
    'valor': lambda x: random.randint(1,10000),
})

seeder.add_entity(Cliente, 50, {
    'nombre': lambda x: seeder.faker.name(),
    'apellido': lambda x: seeder.faker.name(),
    'dateb': lambda x: seeder.faker.date(),
})

seeder.add_entity(Pelicula, 40, {
	'director': lambda x: seeder.faker.name(),
    'titulo': lambda x: seeder.faker.word(),
    'reparto' : lambda x: seeder.faker.word(),
    'calificacion': lambda x: random.randint(1,10),
})

seeder.add_entity(ClientePelicula, 40, {
	'fecha': lambda x: seeder.faker.date(),
	'hora': lambda x: seeder.faker.time(),
	})

seeder.execute()
