from .models import Pelicula
from django.shortcuts import render, redirect

def listarPelicula(request):
	peliculas = Pelicula.objects.all()
	context = {'movie': peliculas}
	return render(request, 'listar.html', context)
