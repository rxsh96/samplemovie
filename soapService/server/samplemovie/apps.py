from django.apps import AppConfig


class SamplemovieConfig(AppConfig):
    name = 'samplemovie'
