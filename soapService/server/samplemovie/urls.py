from django.conf.urls import url
from .views import listarPelicula

urlpatterns = [
    url(r'^$', listarPelicula, name='index'),
]
