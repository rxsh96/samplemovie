from django.shortcuts import render
from spyne import Application, rpc, ServiceBase, Integer, Unicode
from spyne.protocol.http import HttpRpc
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoApplication
from django.views.decorators.csrf import csrf_exempt
from samplemovie.models import Plan

class SoapService(ServiceBase):
    @rpc(_returns=(Unicode))
    def listarPlanes(ctx):
        planes = Plan.objects.all()
        listaPlanes = []
        for plan in planes:
            #print(str(plan.tipo)+': '+str(plan.valor))
            listaPlanes.append(str(plan.tipo)+': '+str(plan.valor))
        return ', '.join(listaPlanes)

soap_app = Application(
    [SoapService],
    tns = 'django.soap.test',
    in_protocol = Soap11(validator = 'lxml'),
    out_protocol = Soap11(),
)

django_soap_application = DjangoApplication(soap_app)
my_soap_application = csrf_exempt(django_soap_application)
