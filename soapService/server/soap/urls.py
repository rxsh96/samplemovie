from django.conf.urls import url
from spyne.protocol.soap import Soap11
from spyne.server.django import DjangoView
from .views import my_soap_application, soap_app, SoapService

urlpatterns = [
    url(r'^my_soap_application/', my_soap_application, name = 'my_soap_application'),
    url(r'^api/', DjangoView.as_view(application = soap_app)),
]
