'''from samplemovie.models import Plan
from zeep import Client

class TipoPlan:
    tipo = ''
    valor = 0
    def __init__(self, tipo, valor):
        self.tipo = tipo
        self.valor = valor
    def planArray(self):
        return{
            'tipo': self.tipo,
            'valor': self.valor,
        }
    def __str__(self):
        return 'Tipo: '+self.tipo+'. Valor: '+self.valor

class Controller:
    client = Client('http://127.0.0.1:8000/soap/my_soap_application/?WSDL')

    def listar(self):
        listaPlanes = []
        lista = self.client.service.listarPlanes()
        lista = lista.split(',')
        for x in lista:
            valores = x.split(':')
            persona = Plan(valores[0], int(valores[1]))
            listaPlanes.append(persona)
        return listaPlanes'''
