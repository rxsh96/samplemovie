from django.conf.urls import url
from .views import listarPelicula, getSoapRequest

urlpatterns = [
    url(r'^$', listarPelicula, name='index'),
    url(r'^planes/$', getSoapRequest, name='planes'),
]
