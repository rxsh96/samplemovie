from .models import Pelicula
from django.shortcuts import render, redirect
from .controller import Controller

def listarPelicula(request):
	peliculas = Pelicula.objects.all()
	context = {'movie': peliculas}
	return render(request, 'listar.html', context)

def getSoapRequest(request):
	controller = Controller()
	plan = controller.listar()
	context = {'plan': plan}
	return render(request, 'listarplan.html', context)
