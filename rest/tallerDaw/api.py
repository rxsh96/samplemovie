from rest_framework import routers
from samplemovie import api_views as myapp_views

router = routers.DefaultRouter()
router.register(r'peliculas', myapp_views.PeliculaViewset)
router.register(r'clientes', myapp_views.ClienteViewset)
