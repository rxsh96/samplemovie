
import requests, json

def listarEntidad(entidad):
    response = requests.get('http://127.0.0.1:8000/api/v1/'+entidad+'/')
    data = response.json()
    return data

def elemInd(entidad, dato):
    response = requests.get('http://127.0.0.1:8000/api/v1/'+entidad+'/'+dato+'/')
    data = response.json()
    return data

def edit(dato, registro):
    response = requests.put('http://127.0.0.1:8000/api/v1/peliculas/'+dato+'/',registro)
    return response

def delete(dato):
    response = requests.delete('http://127.0.0.1:8000/api/v1/peliculas/'+dato+'/')
    return response

def add(registro):
    response = requests.post('http://127.0.0.1:8000/api/v1/peliculas/',registro)
    return response
