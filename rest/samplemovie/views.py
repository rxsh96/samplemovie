from .models import Pelicula, Cliente
from django.shortcuts import render, redirect
from .controller import listarEntidad, edit, elemInd, delete, add

def listarPelicula(request):
	listapeliculas = listarEntidad('peliculas')
	context = {'movie':listapeliculas}
	return render(request, 'listar.html', context)

def infoMovie(request):
	movie = Pelicula.objects.all()[0:10]
	context = {'movie': movie}
	return render(request, 'infoUser.html', context)

def movieDetail(request, dato):
	data = elemInd('peliculas', dato)
	context = {'movie': data}
	print(context)
	return render(request, 'movieDetail.html', context)


def editItem(request, dato):
	data = elemInd('peliculas', dato)
	context = {'pelicula':data['titulo']}
	if request.method == 'POST':
		registro = {'director':data['director'], 'titulo': data['titulo'], 'reparto': data['reparto'], 'calificacion':request.POST.get('calificacion'), 'plan':data['plan']}
		edit(dato, registro)
		return redirect('topten')
	else:
		return render(request, 'editar.html', context)

def deleteItem(request, dato):
	delete(dato)
	return redirect('topten')

def addItem(request):
	if request.method == 'POST':
		registro = {'director':request.POST.get('director'), 'titulo': request.POST.get('titulo'), 'reparto': request.POST.get('reparto'), 'calificacion':int(request.POST.get('calificacion')), 'plan':int(request.POST.get('plan'))}
		add(registro)
		return redirect('topten')
	else:
		return render(request, 'add.html')
