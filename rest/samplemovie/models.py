from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
class Plan(models.Model):
	tipo = models.CharField(max_length=30)
	valor = models.IntegerField()

class Cliente(models.Model):
	nombre = models.CharField(max_length=30)
	apellido = models.CharField(max_length=30)
	dateb = models.DateField()
	ciudad = models.CharField(max_length=60)
	pais = models.CharField(max_length=50)
	address = models.CharField(max_length=50)
	plan = models.ForeignKey(Plan, null=True, blank=True, on_delete=models.CASCADE)

class Pelicula(models.Model):
	director = models.CharField(max_length=50)
	titulo = models.CharField(max_length=50)
	reparto = models.CharField(max_length=300)
	calificacion = models.IntegerField(default=1,validators=[MaxValueValidator(5), MinValueValidator(1)])
	plan = models.ForeignKey(Plan, null=True, blank=True,  on_delete=models.CASCADE)

class ClientePelicula(models.Model):
	cliente = models.ForeignKey(Cliente, null=True, blank=True, on_delete=models.CASCADE)
	pelicula = models.ForeignKey(Pelicula, null=True, blank=True, on_delete=models.CASCADE)
	fecha = models.DateField()
	hora = models.TimeField()
