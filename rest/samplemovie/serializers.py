from rest_framework import serializers
from . import models

class PeliculaSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Pelicula
        fields = ('id', 'director', 'titulo', 'reparto', 'calificacion', 'plan')


class ClienteSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Cliente
        fields = ('nombre', 'apellido', 'dateb', 'ciudad', 'pais', 'address', 'plan')
