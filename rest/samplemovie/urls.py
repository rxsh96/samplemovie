from django.conf.urls import url
from .views import listarPelicula, infoMovie, movieDetail, editItem, deleteItem, addItem

urlpatterns = [
    url(r'^$', listarPelicula, name="topten"),
    url(r'^infoUser$', infoMovie, name="infoUser"),
    url(r'^(?P<dato>\d+)/detalleMovie$', movieDetail, name="movie-info"),
    url(r'^(?P<dato>\d+)/actualizar$', editItem, name='actualizar-model'),
    url(r'^add/$', addItem, name='agregar-model'),
    url(r'^(?P<dato>\d+)/$', deleteItem, name='borrar-model'),
]
