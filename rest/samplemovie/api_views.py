from rest_framework import viewsets
from . import models
from . import serializers

class PeliculaViewset(viewsets.ModelViewSet):
    queryset = models.Pelicula.objects.all()
    serializer_class = serializers.PeliculaSerializer

class ClienteViewset(viewsets.ModelViewSet):
    queryset = models.Cliente.objects.all()
    serializer_class = serializers.ClienteSerializer
